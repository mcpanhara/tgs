class BrandsController < FrontEndApplicationController
  def index

  end

  def show
    @brand = Brand.find_by_slug(params[:id])
    @products = @brand.products.page params[:page]
  end
end
