class SparpartsController < FrontEndApplicationController
  def index
    @categories = Category.all
    @items = Item.all.page params[:page]
  end
end
