class ItemsController < FrontEndApplicationController

  def index
    @search = Product.ransack(params[:search])
    @items = @search.result.order(id: :desc).page params[:page]
    @sparpartbrands = Sparpartbrand.all
    @categories = Category.all
  end

  def show
    @item = Item.find_by_slug(params[:id])
    @items = Item.all
  end
end
