class TypesController < FrontEndApplicationController
  def index
    @types = Type.all.page params[:page]
    @typesofsparpart = Type.find_by_sql("SELECT * FROM types WHERE type_name='sparpart' OR type_name='accessory'")
    @products = Product.all.page params[:page]
    @categories = Type.all
  end

  def show
    @type = Type.find_by_slug(params[:id])
    @typesofproducts = @type.products
    @categories = Type.all
  end
end
