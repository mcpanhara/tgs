class ProductsController < FrontEndApplicationController
  
  def index
    @search = Product.ransack(params[:search])
    @products = @search.result.order(id: :desc).page params[:page]
    @types = Type.all.where.not(type_name: "sparpart").limit(8)
    @brands = Brand.all
  end

  def show
    @product = Product.find_by_slug(params[:id])
    @products = Product.all
  end
end
