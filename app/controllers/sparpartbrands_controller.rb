class SparpartbrandsController < FrontEndApplicationController
  def index
    @items = Item.all.page params[:page]
    @categories = Category.all
  end

  def show
    @sparpartbrand = Sparpartbrand.find_by_slug(params[:id])
    @items = @sparpartbrand.items.page params[:page]
    @categories = Category.all
  end
end
