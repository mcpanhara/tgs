class BlogsController < FrontEndApplicationController
  def index
    @blogs = Blog.all.order(id: :desc).page params[:page]
  end

  def show
    @blog = Blog.find_by_slug(params[:id])
  end
end
