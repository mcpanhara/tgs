class ContactsController < FrontEndApplicationController
  def new
    @contact = Contact.new
  end

  def create
    @contact = Contact.new(contact_params)
    if @contact.save
      redirect_to root_path, notice: "Thank For Contact Us"
    else
      flash[:alert] = "Please fill the field blank"
      render :new
    end
  end


  private


    def contact_params
      params.required(:contact).permit!
    end
end
