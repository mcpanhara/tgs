class FrontEndApplicationController < ActionController::Base
  layout 'front_end_application'
  protect_from_forgery
  before_action :header_nav
  def header_nav
    @new_products = Product.all.order(id: :desc)
    @sparpart = Type.where(type_name: "Sparpart")
    @brands = Brand.all.limit(6)
    @search = Product.ransack(params[:search])
    @products = @search.result.order(id: :desc).limit(15).page params[:page]
    @banners = Banner.all.where(status: true)
    @slideshows = Slideshow.all.where(status: true)
    @bigbanners = Bigbanner.all.where(status: true)
    @blogs = Blog.all
    @settings = Setting.all.limit(1)
    @types = Type.all.limit(8)
    # @typesofsparpart = Type.find_by_sql("SELECT * FROM types WHERE type_name='sparpart' OR type_name='accessory'")
  end
end
