class AboutusController < FrontEndApplicationController
  def index
    @abouts = About.all.limit(1)
  end
end
