class Admin::SettingsController < ApplicationController
  before_action :authenticate_admin_user!
  before_action :admin_only?

  # before_action :set_setting, only: [:destroy]
  def index
    @setting = current_admin_user.settings.build
    @settings = Setting.all
  end

  def show
    @setting = Setting.find_by(params[:id])
  end

  def new

  end

  def create
    @setting = current_admin_user.settings.build(setting_params)
    if @setting.save
      respond_to do |format|
        # flash[:success] = "Successful Created"
        flash.now[:notice] =  " Successful Craeted"
        format.html { redirect_to @setting}
        format.js
      end
    else
      respond_to do |format|
        flash.now[:error] = "Please fill the field blank"
        format.html { redirect_to new_admin_setting_path }
        format.js { render template: 'admin/settings/setting_error.js.erb' }
      end
    end
  end

  def edit
    @setting = Setting.find(params[:id])
    if !@setting.socials.exists?
      1.times do
        @social = @setting.socials.build
      end
    else
      @setting = Setting.find(params[:id])
    end
  end

  def update
    @setting = Setting.find(params[:id])
    if @setting.update(setting_params)
      redirect_to admin_setting_path(@setting)
    else
      render :edit
    end
  end

  def destroy
    @setting = Setting.destroy(params[:id])
    respond_to do |format|
      flash.now[:error] = "Delete"
      format.html { redirect_to admin_settings_url }
      format.js
    end
  end


  private
    def setting_params
      params.required(:setting).permit!
    end

    def admin_only?
      unless current_admin_user.admin?
        unless @user == current_admin_user
          redirect_to admin_dashboard_path, notice: "Access Denied"
        end
      end
    end
end
