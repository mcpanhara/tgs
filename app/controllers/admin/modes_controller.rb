class Admin::ModesController < ApplicationController
  before_action :authenticate_admin_user!
  before_action :admin_only?

  def index
    if Sparpartbrand.exists?
      @mode = current_admin_user.modes.build
    else
      redirect_to admin_sparpartbrands_path
      flash[:error] = "Please input brand first before you get start with your product"
    end
    @modes = Mode.all
  end

  def show
    @mode = Mode.find_by_slug(params[:id])
  end

  def new

  end

  def create
    @mode = current_admin_user.modes.build(mode_params)
    if @mode.save
      respond_to do |format|
        flash.now[:notice] = "Successful Created"
        format.html { redirect_to admin_modes_path }
        format.js
      end
    else
      respond_to do |format|
        flash.now[:alert] = "Please fill the field blank or mode Duplicated"
        format.html { redirect_to admin_modes_path }
        format.js { render template: "admin/modes/mode_error.js.erb" }
      end
    end
  end

  def edit
    @mode = Mode.find_by_slug(params[:id])
  end

  def update
    @mode = Mode.find_by_slug(params[:id])
    if @mode.update(mode_params)
      redirect_to admin_mode_path(@mode), notice: "Successful Updated"
    else
      render :edit
    end
  end

  def destroy
    @mode = Mode.destroy(params[:id])
    respond_to do |format|
      flash.now[:error] = "Delete"
      format.html { redirect_to admin_modes_url }
      format.js
    end
  end

  private
    def admin_only?
      unless  current_admin_user.admin?
        unless @user == current_admin_user
          redirect_to admin_dashboard_path, notice: "Access Denied"
        end
      end
    end

    def mode_params
      params.required(:mode).permit!
    end
end
