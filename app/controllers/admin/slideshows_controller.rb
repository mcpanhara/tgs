class Admin::SlideshowsController < ApplicationController
  before_action :authenticate_admin_user!
  before_action :admin_only?

  def index
    @slideshows = Slideshow.all
  end

  def new
    @slideshow = current_admin_user.slideshows.build
  end

  def create
    @slideshow = current_admin_user.slideshows.build(slideshow_params)
    if @slideshow.save
      redirect_to admin_slideshows_path, notice: "Successfully Created"
    else
      flash.now[:error] = "Please fill the field blank"
      render :new
    end
  end

  def edit
    @slideshow = Slideshow.find_by_slug(params[:id])
  end

  def update
    @slideshow = Slideshow.find_by_slug(params[:id])
    @slideshow.update_attributes!(slideshow_params)

    respond_to do |format|
      flash.now[:notice] = "slideshow Updated"
      format.html{ redirect_to admin_slideshows_path }
      format.js
    end
  end



  private
    def admin_only?
      unless current_admin_user.admin?
        unless @user == current_admin_user
          redirect_to admin_dashboard_path, notice: "Access Denied"
        end
      end
    end

    def slideshow_params
      params.require(:slideshow).permit!
    end
end
