class Admin::ItemsController < ApplicationController
  before_action :authenticate_admin_user!
  before_action :admin_only?

  # before_action :set_item, only: [:destroy]
  def index
    if Sparpartbrand.exists?
      @item = current_admin_user.items.build
      if Category.exists?
        @item = current_admin_user.items.build
          if Mode.exists?
            @item = current_admin_user.items.build
          else
            redirect_to admin_modes_path
            flash[:error] = "Please input mode first before you get start with your item"
          end
      else
        redirect_to admin_categories_path
        flash[:error] = "Please input type first before you get start with your item"
      end
    else
      redirect_to admin_sparpartbrands_path
      flash[:error] = "Please input brand first before you get start with your item"
    end
    @items = Item.all.order(id: :desc)
  end

  def show
    @item = Item.find_by_slug(params[:id])
  end

  def new
    if request.referrer.split("/").last == "items"
      flash[:notice] = nil
    end
    @item = current_admin_user.items.build
  end

  def create
    @item = current_admin_user.items.build(item_params)
    if @item.save
      respond_to do |format|
        # flash[:success] = "Successful Created"
        flash.now[:notice] =  "#{@item.item_name} Successful Craeted"
        format.html { redirect_to @item}
        format.js
      end
    else
      respond_to do |format|
        flash.now[:error] = "Please fill the field blank or item already exist"
        format.html { redirect_to new_admin_item_path }
        format.js { render template: 'admin/items/item_error.js.erb' }
      end
    end
  end

  def edit
    @item = Item.find_by_slug(params[:id])
  end

  def update
    @item = Item.find_by_slug(params[:id])
    if @item.update(item_params)
      respond_to do |format|
        flash[:notice] = "item Updated"
        format.html{ redirect_to admin_item_path(@item) }
        format.js
      end
    else
      render :edit
    end

  end

  def destroy
    @item = Item.destroy(params[:id])
    respond_to do |format|
      flash.now[:error] = "Delete"
      format.html { redirect_to admin_items_url }
      format.js
    end
  end


  private
    def item_params
      params.required(:item).permit!
    end

    def admin_only?
      unless current_admin_user.admin?
        unless @user == current_admin_user
          redirect_to admin_dashboard_path, notice: "Access Denied"
        end
      end
    end
end
