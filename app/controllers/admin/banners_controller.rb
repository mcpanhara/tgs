class Admin::BannersController < ApplicationController
  before_action :authenticate_admin_user!
  before_action :admin_only?

  # before_action :set_banner, only: [:destroy]
  def index
    @banner = current_admin_user.banners.build
    @banners = Banner.all
  end

  def show
    @banner = Banner.find_by_slug(params[:id])
  end

  def new

  end

  def create
    @banner = current_admin_user.banners.build(banner_params)
    if @banner.save
      respond_to do |format|
        # flash[:success] = "Successful Created"
        flash.now[:notice] =  "#{@banner.title} Successful Craeted"
        format.html { redirect_to @banner}
        format.js
      end
    else
      respond_to do |format|
        flash.now[:error] = "Please fill the field blank"
        format.html { redirect_to new_admin_banner_path }
        format.js { render template: 'admin/banners/banner_error.js.erb' }
      end
    end
  end

  def edit
    @banner = Banner.find_by_slug(params[:id])
  end

  def update
    @banner = Banner.find_by_slug(params[:id])
    if @banner.update(banner_params)
      respond_to do |format|
        flash.now[:notice] = "banner Updated"
        format.html{ redirect_to admin_banner_path(@banner) }
        format.js
      end
    else
      render :edit
    end
  end

  def destroy
    @banner = Banner.destroy(params[:id])
    respond_to do |format|
      flash.now[:error] = "Delete"
      format.html { redirect_to admin_banners_url }
      format.js
    end
  end


  private
    def banner_params
      params.required(:banner).permit!
    end

    def admin_only?
      unless current_admin_user.admin?
        unless @user == current_admin_user
          redirect_to admin_dashboard_path, notice: "Access Denied"
        end
      end
    end

end
