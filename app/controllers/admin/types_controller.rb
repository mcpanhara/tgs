class Admin::TypesController < ApplicationController

  before_action :authenticate_admin_user!
  before_action :admin_only?

  def index
    if Brand.exists?
      @type = current_admin_user.types.build
    else
      redirect_to admin_brands_path
      flash[:error] = "Please input brand first before you get start with your product"
    end
    @types = Type.all
  end

  def show
    @type = Type.find_by_slug(params[:id])
  end

  def new

  end

  def create
    @type = current_admin_user.types.build(type_params)
    if @type.save
      respond_to do |format|
        flash.now[:notice] = "Successful Created"
        format.html { redirect_to admin_types_path }
        format.js
      end
    else
      respond_to do |format|
        flash.now[:alert] = "Please fill the field blank or Type Duplicated"
        format.html { redirect_to admin_types_path }
        format.js { render template: "admin/types/type_error.js.erb" }
      end
    end
  end

  def edit
    @type = Type.find_by_slug(params[:id])
  end

  def update
    @type = Type.find_by_slug(params[:id])
    if @type.update(type_params)
      redirect_to admin_type_path(@type), notice: "Successful Updated"
    else
      render :edit
    end
  end

  def destroy
    @type = Type.destroy(params[:id])
    respond_to do |format|
      flash.now[:error] = "Delete"
      format.html { redirect_to admin_types_url }
      format.js
    end
  end

  private
    def admin_only?
      unless  current_admin_user.admin?
        unless @user == current_admin_user
          redirect_to admin_dashboard_path, notice: "Access Denied"
        end
      end
    end

    def type_params
      params.required(:type).permit!
    end
end
