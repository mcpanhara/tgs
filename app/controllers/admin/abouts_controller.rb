class Admin::AboutsController < ApplicationController
  before_action :authenticate_admin_user!
  before_action :admin_only?

  # before_action :set_about, only: [:destroy]
  def index
    @about = current_admin_user.abouts.build
    @abouts = About.all
  end

  def show
    @about = About.find_by_slug(params[:id])
  end

  def new

  end

  def create
    @about = current_admin_user.abouts.build(about_params)
    if @about.save
      respond_to do |format|
        # flash[:success] = "Successful Created"
        flash.now[:notice] =  "Successful Craeted"
        format.html { redirect_to @about}
        format.js
      end
    else
      respond_to do |format|
        flash.now[:error] = "Please fill the field blank"
        format.html { redirect_to new_admin_about_path }
        format.js { render template: 'admin/abouts/about_error.js.erb' }
      end
    end
  end

  def edit
    @about = About.find_by_slug(params[:id])
  end

  def update
    @about = About.find_by_slug(params[:id])
    if @about.update(about_params)
      respond_to do |format|
        flash[:notice] = "about Updated"
        format.html{ redirect_to admin_about_path(@about) }
        format.js
      end
    else
      render :edit
    end
  end

  def destroy
    @about = About.destroy(params[:id])
    respond_to do |format|
      flash.now[:error] = "Delete"
      format.html { redirect_to admin_abouts_url }
      format.js
    end
  end


  private
    def about_params
      params.required(:about).permit!
    end

    def admin_only?
      unless current_admin_user.admin?
        unless @user == current_admin_user
          redirect_to admin_dashboard_path, notice: "Access Denied"
        end
      end
    end

end
