class Admin::BigbannersController < ApplicationController
  before_action :authenticate_admin_user!
  before_action :admin_only?

  def index
    @bigbanner = current_admin_user.bigbanners.build
    @bigbanners = Bigbanner.all
  end

  def show
    @bigbanner = Bigbanner.find_by_slug(params[:id])
  end

  def new
    if request.referrer.split("/").last == "bigbanners"
      flash[:notice] = nil
    end
    @bigbanner = current_admin_user.bigbanners.build
  end

  def create
    @bigbanner = current_admin_user.bigbanners.build(bigbanner_params)
    if @bigbanner.save
      respond_to do |format|
        # flash[:success] = "Successful Created"
        flash.now[:notice] =  "#{@bigbanner.title} Successful Craeted"
        format.html { redirect_to @bigbanner}
        format.js
      end
    else
      respond_to do |format|
        flash.now[:error] = "Please fill the field blank"
        format.html { redirect_to new_admin_bigbanner_path }
        format.js { render template: 'admin/bigbanners/bigbanner_error.js.erb' }
      end
    end
  end

  def edit
    @bigbanner = Bigbanner.find_by_slug(params[:id])
  end

  def update
    @bigbanner = Bigbanner.find_by_slug(params[:id])
    if @bigbanner.update(bigbanner_params)
      respond_to do |format|
        flash[:notice] = "bigbanner Updated"
        format.html{ redirect_to admin_bigbanner_path(@bigbanner) }
        format.js
      end
    else
      render :edit
    end
  end

  def destroy
    @bigbanner = Bigbanner.destroy(params[:id])
    respond_to do |format|
      flash.now[:error] = "Delete"
      format.html { redirect_to admin_bigbanners_url }
      format.js
    end
  end

  private
    def admin_only?
      unless current_admin_user.admin?
        unless @user == current_admin_user
          redirect_to admin_dashboard_path, notice: "Access Denied"
        end
      end
    end

    def bigbanner_params
      params.require(:bigbanner).permit!
    end
end
