class Admin::BlogsController < ApplicationController
  before_action :authenticate_admin_user!
  before_action :admin_only?

  # before_action :set_blog, only: [:destroy]
  def index
    @blog = current_admin_user.blogs.build
    @blogs = Blog.all
  end

  def show
    @blog = Blog.find_by_slug(params[:id])
  end

  def new

  end

  def create
    @blog = current_admin_user.blogs.build(blog_params)
    if @blog.save
      respond_to do |format|
        # flash[:success] = "Successful Created"
        flash.now[:notice] =  "Successful Craeted"
        format.html { redirect_to @blog}
        format.js
      end
    else
      respond_to do |format|
        flash.now[:error] = "Please fill the field blank"
        format.html { redirect_to new_admin_blog_path }
        format.js { render template: 'admin/blogs/blog_error.js.erb' }
      end
    end
  end

  def edit
    @blog = Blog.find_by_slug(params[:id])
  end

  def update
    @blog = Blog.find_by_slug(params[:id])
    if @blog.update(blog_params)
      respond_to do |format|
        flash[:notice] = "blog Updated"
        format.html{ redirect_to admin_blog_path(@blog) }
        format.js
      end
    else
      render :edit
    end
  end

  def destroy
    @blog = blog.destroy(params[:id])
    respond_to do |format|
      flash.now[:error] = "Delete"
      format.html { redirect_to admin_blogs_url }
      format.js
    end
  end


  private
    def blog_params
      params.required(:blog).permit!
    end

    def admin_only?
      unless current_admin_user.admin?
        unless @user == current_admin_user
          redirect_to admin_dashboard_path, notice: "Access Denied"
        end
      end
    end

end
