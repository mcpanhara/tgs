class Admin::SparpartbrandsController < ApplicationController
  before_action :authenticate_admin_user!
  before_action :admin_only?

  def index
    @sparpartbrands = Sparpartbrand.all
    @sparpartbrand = Sparpartbrand.new
  end

  def show
    @sparpartbrand = Sparpartbrand.find_by_slug(params[:id])
  end

  def new
  end

  def create
    @sparpartbrand = Sparpartbrand.new(sparpartbrand_params)
    if @sparpartbrand.save
      respond_to do |format|
        flash.now[:notice] = "Successfully Created"
        format.html{ redirect_to admin_sparpartbrands_path}
        format.js
      end
    else
      respond_to do |format|
        flash.now[:error] = "Please fill the field blank"
        format.html{ redirect_to admin_sparpartbrands_path}
        format.js{ render template: "admin/sparpartbrands/sparpartbrand_error.js.erb" }
      end
    end
  end

  def edit
    @sparpartbrand = Sparpartbrand.find_by_slug(params[:id])
  end

  def update
    @sparpartbrand = Sparpartbrand.find_by_slug(params[:id])
    if @sparpartbrand.update(sparpartbrand_params)
      redirect_to admin_sparpartbrand_path(@sparpartbrand), notice: "Successfully Updated"
    else
      render :edit
    end
  end


    def destroy
      @sparpartbrand = Sparpartbrand.destroy(params[:id])
      respond_to do |format|
        flash.now[:error] = "Delete"
        format.html { redirect_to admin_sparpartbrands_url }
        format.js
      end
    end


  private

    def admin_only?
      unless current_admin_user.admin?
        unless @user == current_admin_user
          redirect_to admin_dashboard_path, notice: "Access Denied"
        end
      end
    end

    def sparpartbrand_params
      params.required(:sparpartbrand).permit(:brand_name)
    end
end
