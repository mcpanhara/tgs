class Admin::ProductsController < ApplicationController
  before_action :authenticate_admin_user!
  before_action :admin_only?

  # before_action :set_product, only: [:destroy]
  def index
    if Brand.exists?
      @product = current_admin_user.products.build
      1.times do
        @prop = @product.properties.build
      end
      if Type.exists?
        @product = current_admin_user.products.build
        1.times do
          @prop = @product.properties.build
        end
      else
        redirect_to admin_types_path
        flash[:error] = "Please input type first before you get start with your product"
      end
    else
      redirect_to admin_brands_path
      flash[:error] = "Please input brand first before you get start with your product"
    end
    @products = Product.all.order(id: :desc)
  end

  def show
    @product = Product.find_by_slug(params[:id])
  end

  def new
    if request.referrer.split("/").last == "products"
      flash[:notice] = nil
    end
    @product = current_admin_user.products.build
  end

  def create
    @product = current_admin_user.products.build(product_params)
    if @product.save
      respond_to do |format|
        # flash[:success] = "Successful Created"
        flash.now[:notice] =  "#{@product.product_name} Successful Craeted"
        format.html { redirect_to @product}
        format.js
      end
    else
      respond_to do |format|
        flash.now[:error] = "Please fill the field blank or product already exist"
        format.html { redirect_to new_admin_product_path }
        format.js { render template: 'admin/products/product_error.js.erb' }
      end
    end
  end

  def edit
    @product = Product.find_by_slug(params[:id])
  end

  def update
    @product = Product.find_by_slug(params[:id])
    if @product.update(product_params)
      respond_to do |format|
        flash[:notice] = "Product Updated"
        format.html{ redirect_to admin_product_path(@product) }
        format.js
      end
    else
      render :edit
    end

  end

  def destroy
    @product = Product.destroy(params[:id])
    respond_to do |format|
      flash.now[:error] = "Delete"
      format.html { redirect_to admin_products_url }
      format.js
    end
  end


  private
    def product_params
      params.required(:product).permit!
    end

    def admin_only?
      unless current_admin_user.admin?
        unless @user == current_admin_user
          redirect_to admin_dashboard_path, notice: "Access Denied"
        end
      end
    end
end
