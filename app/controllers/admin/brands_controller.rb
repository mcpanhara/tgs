class Admin::BrandsController < ApplicationController
  before_action :authenticate_admin_user!
  before_action :admin_only?

  def index
    @brands = Brand.all
    @brand = Brand.new
  end

  def show
    @brand = Brand.find_by_slug(params[:id])
  end

  def new
  end

  def create
    @brand = Brand.new(brand_params)
    if @brand.save
      respond_to do |format|
        flash.now[:notice] = "Successfully Created"
        format.html{ redirect_to admin_brands_path}
        format.js
      end
    else
      respond_to do |format|
        flash.now[:error] = "Please fill the field blank or duplicate data"
        format.html{ redirect_to admin_brands_path}
        format.js{ render template: "admin/brands/brand_error.js.erb" }
      end
    end
  end

  def edit
    @brand = Brand.find_by_slug(params[:id])
  end

  def update
    @brand = Brand.find_by_slug(params[:id])
    if @brand.update(brand_params)
      redirect_to admin_brand_path(@brand), notice: "Successfully Updated"
    else
      render :edit
    end
  end


    def destroy
      @brand = Brand.destroy(params[:id])
      respond_to do |format|
        flash.now[:error] = "Delete"
        format.html { redirect_to admin_brands_url }
        format.js
      end
    end


  private

    def admin_only?
      unless current_admin_user.admin?
        unless @user == current_admin_user
          redirect_to admin_dashboard_path, notice: "Access Denied"
        end
      end
    end

    def brand_params
      params.required(:brand).permit(:brand_name)
    end
end
