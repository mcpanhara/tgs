class HomesController < FrontEndApplicationController

  def index
    @contact = Contact.new
  end



    def create
      @contact = Contact.new(contact_params)
      if @contact.save
        respond_to do |format|
          flash.now[:notice] =  "Thank For Submit Your Infomation"
          format.html { redirect_to root_path }
          format.js { render template: '/homes/contact_success.js.erb' }
        end
      else
        respond_to do |format|
          flash.now[:error] = "Message did not send."
          format.html { render 'index' }
          format.js   { render template: '/homes/contact_error.js.erb' }
        end
      end
    end


    private


      def contact_params
        params.required(:contact).permit!
      end

end
