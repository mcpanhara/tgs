class ModesController < FrontEndApplicationController
  def index
    @items = Item.all.page params[:page]
    @categories = Category.all
  end

  def show
    @mode = Mode.find_by_slug(params[:id])
    @items = @mode.items.page params[:page]
    @categories = Category.all
  end
end
