class Category < ApplicationRecord
  before_save :update_slug
  has_many :items, dependent: :destroy
  has_many :sparpartbrands, :through => :items, dependent: :destroy
  belongs_to :user

  def update_slug
    self.slug = category_name.parameterize
  end

  def to_param
    slug
  end

  validates :category_name, presence: true
  validates :category_name, uniqueness: {case_sensitive: false}
end
