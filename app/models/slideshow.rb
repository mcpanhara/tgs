class Slideshow < ApplicationRecord
  belongs_to :user
  before_save :update_slug

  def update_slug
    self.slug = title.parameterize
  end

  def to_param
    slug
  end

  validates :title, presence: true
  validates :title , uniqueness: {case_sensitive: false}
  has_attached_file :image, style: { medium: "300x500>",thumb: "100x100>" }
  has_attached_file :background, style: { medium: "300x500>",thumb: "100x100>" }
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/
  validates_attachment_content_type :background, content_type: /\Aimage\/.*\z/
end
