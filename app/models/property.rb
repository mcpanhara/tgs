class Property < ApplicationRecord
  belongs_to :product
  belongs_to :user, required: false
  before_save :update_slug

  def update_slug
    self.slug = color.parameterize
  end

  def to_param
    slug
  end

  validates :color, presence: true
  validates :color , uniqueness: {case_sensitive: false}
end
