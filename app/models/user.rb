class User < ApplicationRecord
  before_save :update_slug
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :lockable

  enum role: [:user, :vip, :admin]
  after_initialize :set_default_role, :if => :new_record?

  def set_default_role
    self.role ||= :user
  end

  def update_slug
    self.slug = username.parameterize
  end

  def to_param
    slug
  end

  validates :username, presence: true
  validates :username , uniqueness: {case_sensitive: false}

  has_attached_file :image, style: { medium: "300x500>",thumb: "100x100>" }
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/

  has_many :products
  has_many :properties
  has_many :types
  has_many :banners
  has_many :slideshows
  has_many :bigbanners
  has_many :abouts
  has_many :blogs
  has_many :settings
  has_many :socials
  has_many :categories
  has_many :items
  has_many :modes
end
