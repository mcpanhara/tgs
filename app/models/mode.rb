class Mode < ApplicationRecord
  before_save :update_slug

  has_many :items, dependent: :destroy
  belongs_to :user


  def update_slug
    self.slug = mode_name.parameterize
  end

  def to_param
    slug
  end

  validates :mode_name, presence: true
  validates :mode_name, uniqueness: {case_sensitive: false}
end
