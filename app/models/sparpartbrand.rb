class Sparpartbrand < ApplicationRecord
  before_save :update_slug

  has_many :items, dependent: :destroy
  has_many :categories, :through => :items, dependent: :destroy
  
  def update_slug
    self.slug = brand_name.parameterize
  end

  def to_param
    slug
  end

  validates :brand_name, presence: true
  validates :brand_name , uniqueness: {case_sensitive: false}
end
