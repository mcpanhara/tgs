class Social < ApplicationRecord
  belongs_to :setting
  belongs_to :user, required: false

  SECTION_SELECT_SOCIAL =[
    "facebook",
    "instagram",
    "google",
    "youtube",
    "tweet"
  ]

end
