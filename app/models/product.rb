class Product < ApplicationRecord
  belongs_to :brand
  belongs_to :type, required: false
  belongs_to :user, required: false
  has_many :properties, inverse_of: :product
  accepts_nested_attributes_for :properties, :reject_if => :all_blank, :allow_destroy => true

  before_save :update_slug
  validates :product_name, presence: true
  validates :product_name, uniqueness: {case_sensitive: false}
  def update_slug
    self.slug = product_name.parameterize
  end

  def to_param
    slug
  end

  validates :product_name, presence: true
  validates :product_name , uniqueness: {case_sensitive: false}

  has_attached_file :image, style: { medium: "300x500>",thumb: "100x100>" }
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/
end
