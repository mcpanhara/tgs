class Setting < ApplicationRecord
  belongs_to :user
  has_many :socials, inverse_of: :setting
  accepts_nested_attributes_for :socials, :reject_if => :all_blank, :allow_destroy => true

  has_attached_file :image, style: { medium: "300x500>",thumb: "100x100>" }
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/
end
