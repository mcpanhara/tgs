class Banner < ApplicationRecord
  before_save :update_slug
  belongs_to :user

  def update_slug
    self.slug = title.parameterize
  end

  def to_param
    slug
  end

  validates :title, presence: true
  validates :title , uniqueness: {case_sensitive: false}
  has_attached_file :image, style: { medium: "300x500>",thumb: "100x100>" }
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/
end
