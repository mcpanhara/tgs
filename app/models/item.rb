class Item < ApplicationRecord
  before_save :update_slug
  belongs_to :category
  belongs_to :sparpartbrand
  belongs_to :user
  belongs_to :mode
  def update_slug
    self.slug = item_name.parameterize
  end

  def to_param
    slug
  end

  validates :item_name, presence: true
  validates :item_name, uniqueness: {case_sensitive: false}

  has_attached_file :image, style: { medium: "300x500>",thumb: "100x100>" }
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/
end
