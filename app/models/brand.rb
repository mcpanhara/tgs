class Brand < ApplicationRecord
  has_many :products, dependent: :destroy
  has_many :types, :through => :products, dependent: :destroy
  has_many :items, dependent: :destroy
  has_many :categories, :through => :items, dependent: :destroy
  before_save :update_slug
  def update_slug
    self.slug = brand_name.parameterize
  end

  def to_param
    slug
  end

  validates :brand_name, presence: true
  validates :brand_name , uniqueness: {case_sensitive: false}
end
