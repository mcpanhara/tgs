class Type < ApplicationRecord
  scope :sparpart, -> { where type_name: 'Sparpart' }
  scope :accessory, -> { where type_name: 'Accessory' }

  before_save :update_slug
  has_many :products, inverse_of: :type, dependent: :destroy
  accepts_nested_attributes_for :products, :reject_if => :all_blank, :allow_destroy => true
  has_many :brands, :through => :products, dependent: :destroy
  belongs_to :user, required: false

  validates :type_name, presence: true


  def update_slug
    self.slug = type_name.parameterize
  end

  def to_param
    slug
  end

  validates :type_name, presence: true
  validates :type_name, uniqueness: {case_sensitive: false}

end
