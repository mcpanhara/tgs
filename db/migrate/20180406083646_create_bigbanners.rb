class CreateBigbanners < ActiveRecord::Migration[5.1]
  def change
    create_table :bigbanners do |t|
      t.string :title
      t.string :subtitle
      t.integer :user_id
      t.string :slug
      t.string :image

      t.timestamps
    end
  end
end
