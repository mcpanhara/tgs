class CreateSocials < ActiveRecord::Migration[5.1]
  def change
    create_table :socials do |t|
      t.string :socialm
      t.string :url
      t.integer :user_id
      t.integer :setting_id

      t.timestamps
    end
  end
end
