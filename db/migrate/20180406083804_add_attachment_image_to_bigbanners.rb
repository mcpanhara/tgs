class AddAttachmentImageToBigbanners < ActiveRecord::Migration[5.1]
  def self.up
    change_table :bigbanners do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :bigbanners, :image
  end
end
