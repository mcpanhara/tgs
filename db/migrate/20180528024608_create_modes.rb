class CreateModes < ActiveRecord::Migration[5.1]
  def change
    create_table :modes do |t|
      t.string :mode_name
      t.integer :user_id
      t.string :slug

      t.timestamps
    end
  end
end
