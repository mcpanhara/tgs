class CreateItems < ActiveRecord::Migration[5.1]
  def change
    create_table :items do |t|
      t.string :item_name
      t.string :price
      t.string :category_id
      t.text :description
      t.string :color
      t.boolean :status
      t.integer :user_id

      t.timestamps
    end
  end
end
