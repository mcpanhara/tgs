class CreateSparpartbrands < ActiveRecord::Migration[5.1]
  def change
    create_table :sparpartbrands do |t|
      t.string :brand_name

      t.timestamps
    end
  end
end
