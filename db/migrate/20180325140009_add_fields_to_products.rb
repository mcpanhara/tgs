class AddFieldsToProducts < ActiveRecord::Migration[5.1]
  def change
    add_column :products, :product_price, :decimal
    add_column :products, :product_qty, :integer
    add_column :products, :slug, :string
    add_column :products, :type_id, :integer
    add_column :products, :user_id, :integer
  end
end
