class AddStatusToBigbanner < ActiveRecord::Migration[5.1]
  def change
    add_column :bigbanners, :status, :boolean
  end
end
