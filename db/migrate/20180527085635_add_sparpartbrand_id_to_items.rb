class AddSparpartbrandIdToItems < ActiveRecord::Migration[5.1]
  def change
    add_column :items, :sparpartbrand_id, :integer
  end
end
