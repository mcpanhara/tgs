class AddAttachmentBackgroundToSlideshows < ActiveRecord::Migration[5.1]
  def self.up
    change_table :slideshows do |t|
      t.attachment :background
    end
  end

  def self.down
    remove_attachment :slideshows, :background
  end
end
