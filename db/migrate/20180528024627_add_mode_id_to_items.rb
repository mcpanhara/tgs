class AddModeIdToItems < ActiveRecord::Migration[5.1]
  def change
    add_column :items, :mode_id, :integer
  end
end
