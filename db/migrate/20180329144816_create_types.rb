class CreateTypes < ActiveRecord::Migration[5.1]
  def change
    create_table :types do |t|
      t.string :type_name
      t.integer :user_id
      t.string :slug
      t.timestamps
    end
  end
end
