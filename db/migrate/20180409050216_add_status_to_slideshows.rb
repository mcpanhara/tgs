class AddStatusToSlideshows < ActiveRecord::Migration[5.1]
  def change
    add_column :slideshows, :status, :boolean
  end
end
