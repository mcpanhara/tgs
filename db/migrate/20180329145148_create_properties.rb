class CreateProperties < ActiveRecord::Migration[5.1]
  def change
    create_table :properties do |t|
      t.string :color
      t.string :weight
      t.string :voltage
      t.string :slug
      t.integer :product_id
      t.integer :user_id
      t.timestamps
    end
  end
end
