class CreateSparparts < ActiveRecord::Migration[5.1]
  def change
    create_table :sparparts do |t|
      t.string :name
      t.text :description
      t.string :image
      t.string :slug
      t.integer :user_id
      t.integer :type_id
      t.integer :brand_id
      t.string :color
      t.boolean :status

      t.timestamps
    end
  end
end
