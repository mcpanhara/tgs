class AddSlugToSparpartbrands < ActiveRecord::Migration[5.1]
  def change
    add_column :sparpartbrands, :slug, :string
  end
end
