class CreateBanners < ActiveRecord::Migration[5.1]
  def change
    create_table :banners do |t|
      t.string :title
      t.string :subtitle
      t.string :image
      t.boolean :status
      t.integer :user_id
      t.string :slug

      t.timestamps
    end
  end
end
