class AddAttachmentImageToSparparts< ActiveRecord::Migration[5.1]
  def self.up
    change_table :sparparts do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :sparparts, :image
  end
end
