class AddSlugToAbouts < ActiveRecord::Migration[5.1]
  def change
    add_column :abouts, :slug, :text
  end
end
