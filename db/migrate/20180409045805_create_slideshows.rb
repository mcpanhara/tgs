class CreateSlideshows < ActiveRecord::Migration[5.1]
  def change
    create_table :slideshows do |t|
      t.string :title
      t.string :subtitle
      t.string :url
      t.string :background
      t.string :image
      t.integer :user_id

      t.timestamps
    end
  end
end
