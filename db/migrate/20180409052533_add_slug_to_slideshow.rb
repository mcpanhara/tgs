class AddSlugToSlideshow < ActiveRecord::Migration[5.1]
  def change
    add_column :slideshows, :slug, :string
  end
end
