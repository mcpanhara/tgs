class AddFieldToSparparts < ActiveRecord::Migration[5.1]
  def change
    add_column :sparparts, :price, :decimal
  end
end
