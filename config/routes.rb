Rails.application.routes.draw do
  # devise_for :users
  namespace :admin do
    root "dashboard#index"
    get "dashboard" => "dashboard#index", :as => :dashboard

    devise_scope :user do
      get "/sign_in" => "devise/sessions#new" # custom path to login/sign_in
      get "/sign_up" => "devise/registrations#new", as: "new_user_registration" # custom path to sign_up/registration
    end

    devise_for :users, controllers: { sessions: 'admin/users/sessions'}, :passwords => 'admin/users/passwords', :skip => [:registrations]

    as :user do
      get 'users/edit' => 'devise/registrations#edit', :as => 'edit_user_registration'
      put 'users' => 'devise/registrations#update', :as => 'user_registration'
      patch 'users' => 'devise/registrations#update', :as => 'update_user_registration'
    end

    resources :products
    resources :quicks
    resources :types
    resources :brands
    resources :banners
    resources :slideshows
    resources :bigbanners
    resources :users
    resources :abouts
    resources :blogs
    resources :settings
    resources :items
    resources :categories
    resources :sparpartbrands
    resources :modes
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root "homes#index"
  resources :homes
  resources :products, only: [:index, :show]
  resources :brands, only: [:index, :show]
  resources :types, only: [:index, :show]
  resources :contacts, only: [:new, :create]
  resources :aboutus, only: [:index]
  resources :blogs, only: [:index, :show]
  resources :sparparts, only: [:index, :show]
  resources :items, only: [:index, :show]
  resources :sparpartbrands, only: [:index, :show]
  resources :modes, only: [:index, :show]
end
